const path = require('path');
const ExtractTextWebpackPlugin = require("extract-text-webpack-plugin");
const HtmlWebpackPlugin = require("html-webpack-plugin");


let config = {
  entry: "./index.js",
  mode: "development",
  output: {
    path: path.resolve(__dirname, "./public"),
    filename: "./bundle.js"
  },
  resolve: {
    extensions: ['.js', '.scss', '.css', '.ejs'],
    alias:{
      "@noa/base-style" : path.resolve(__dirname, '../base/'),
      "@noa/custom-style" : path.resolve(__dirname, '../custom/')
    }
  },
  module: {
    rules: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: "babel-loader"
    },
      {
        test: /\.scss$/,
        use: ['css-hot-loader'].concat(ExtractTextWebpackPlugin.extract({
          fallback: 'style-loader',
          use: ['css-loader', 'sass-loader', 'postcss-loader']
        }))
      },
      {
        test: /\.html$/,
        loader: "raw-loader"
      },
      {
        test: /\.ejs$/,
        loader: "ejs-loader"
      }
    ]
  },
  plugins: [
    new ExtractTextWebpackPlugin("styles.css"),
    new HtmlWebpackPlugin({template: './views/index.ejs'})
  ],
  devServer: {
    contentBase: path.resolve(__dirname, "./public"),
    historyApiFallback: true,
    inline: true,
    open: true,
    hot: true,
    overlay: true
  },
  devtool: "eval-source-map"
};

module.exports = config;
